package br.com.torresti.musica.util;

import br.com.torresti.musica.util.MusicaConstantes.TipoSite;
import br.com.torresti.spring.exception.BusinessException;

public class MusicaUtil {
	
	
	public static String obterUrlToId(Integer tipo, String id) throws BusinessException {
		
		if (TipoSite.Juno.getTipo() == tipo) {
			return "https://www.juno.co.uk/products/" + id + "/";
		}
		
		if (TipoSite.Discogs.getTipo() == tipo) {
			return "https://api.discogs.com/releases/" + id;
		}

		if (TipoSite.Decks.getTipo() == tipo) {
			return "https://www.decks.de/decks/workfloor/lists/findTrack.php?code=" + id;
		}
		
		if (TipoSite.DeejayDe.getTipo() == tipo) {
			return "https://www.deejay.de/" + id;
		}
				
		throw new BusinessException("Tipo inv�lido");
		
	}
	
	public static String obterIdToURL(Integer tipo, String url) throws BusinessException {
		
		if (TipoSite.Juno.getTipo() == tipo) {
			return url.replace("https://www.juno.co.uk/products/","").replace("/", "");
		}
		
		if (TipoSite.Discogs.getTipo() == tipo) {
			return url.replace("https://api.discogs.com/releases/","");
		}

		if (TipoSite.Decks.getTipo() == tipo) {
			return url.replace("https://www.decks.de/decks/workfloor/lists/findTrack.php?code=","");
		}
		
		if (TipoSite.DeejayDe.getTipo() == tipo) {
			return url.replace("https://www.deejay.de/","");
		}
		
		throw new BusinessException("Tipo inv�lido");
		
	}
	
}
