package br.com.torresti.musica.util;

import org.apache.commons.lang3.StringEscapeUtils;

import br.com.torresti.musica.util.to.ArtistaMusicaVO;

public class ArtistaMusicaUtil {

	public static ArtistaMusicaVO obterArtistaMusicaVO(String artistaPrincipal, String musica) {
		
		String artista = artistaPrincipal;
		
		musica = StringEscapeUtils.unescapeHtml4(musica);
		
		if (musica.indexOf("-") != -1) {
		
			artista = ArtistaMusicaUtil.ajustarArtista(musica.split("-")[0].trim());
		
			musica = musica.split("-")[1].trim();
			
		}
		
		musica = ArtistaMusicaUtil.ajustarMusica(StringUtil.removerLast(musica.replace("\"", ""), "(", ")"), false);
		
		return new ArtistaMusicaVO(artista, musica);
		
	}	
	
	public static String ajustarArtista(String valor) {
		
		valor = valor.replace(", ", " & ");
		
		valor = valor.replace(" ,", " & ");
		
		valor = valor.replace("!", "");
		
		valor = valor.replace(".", "");
		
		valor = valor.replace("(", "");
		
		valor = valor.replace(")", "");
		
		valor = valor.replace("'", "");
		
		valor = removerLadoDisco(valor);
		
		valor = StringUtil.removerAcentos(valor);
		
		valor = valor.toLowerCase();
		
		return StringUtil.ajustarTextoMaiusculo(valor).trim();
		
	}

	public static String ajustarAlbum(String valor) {
		
		if (valor == null) {
			
			valor = "No Name";
			
		}
		
		valor = valor.replaceAll("_","");
		
		valor = valor.replace(".","");
		
		valor = valor.replace("'","");
		
		valor = valor.replace("[","");
		
		valor = valor.replace("]","");
		
		valor = valor.replace("(","");
		
		valor = valor.replace(")","");
		
		valor = valor.replace("{","");
		
		valor = valor.replace("}","");
		
		valor = valor.replace("?","");
		
		valor = valor.replace("!","");
		
		valor = valor.replace("#","");		
		
		valor = StringUtil.removerAcentos(valor);
		
		valor = valor.toLowerCase();
		
		valor = valor.replace("vinyl only","");
		
		valor = valor.replace("  "," ");
		
		return StringUtil.ajustarTextoMaiusculo(valor).trim();
		
	}
	
	public static String ajustarSelo(String valor) {
		
		valor = valor.replaceAll("_","");
		
		valor = valor.replace("?","");
		
		valor = valor.replace("!","");
		
		valor = valor.replace("#","");
		
		valor = valor.replace("'","");
		
		valor = StringUtil.removerAcentos(valor);
		
		valor = valor.toLowerCase();
		
		return StringUtil.ajustarTextoMaiusculo(valor).trim();
		
	}
			
	public static String removerLadoDisco(String valor) {
		
		valor = valor + " ";
		
		String[] lados = {"A", "B", "C", "D", "E", "F", "G", "H"}; 
		
		for (String lado : lados) {
			
			valor = removerLadoDisco(valor, lado);
			
		}
		
		return valor.trim();
		
		
	}
	
	public static String removerLadoDisco(String valor, String letra) {
		
		for (int i = 0; i < 11; i++) {
			
			if (valor.indexOf(letra + i + " ") != -1) {
				
				return valor.replace(letra + i + " ", "");
				
			}
			
		}
		
		return valor;
	}
	
	public static String ajustarMusica(String valor, boolean editarLadoMusica) {
		
		valor = valor.replace(", ", " & ");
		
		valor = valor.replace(" ,", " & ");
		
		valor = valor.replace("!", "");
		
		valor = valor.replace("'", "");
		
		valor = valor.replace(".", " ");
				
		valor = valor.replace(" )", ")");

		if (editarLadoMusica) {
		
			valor = ArtistaMusicaUtil.removerLadoDisco(valor);
			
		}
		
		valor = StringUtil.removerAcentos(valor);
		
		valor = valor.toLowerCase();
		
		valor = valor.replace("(original mix)","");
		
		valor = valor.replace("vinyl only","");
		
		valor = valor.replace(" )",")");
		
		valor = valor.toLowerCase();
		
		return StringUtil.ajustarTextoMaiusculo(valor).trim();
		
	}
	
}
