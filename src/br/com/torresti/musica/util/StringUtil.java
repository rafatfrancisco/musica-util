package br.com.torresti.musica.util;

import java.text.Normalizer;

import org.apache.commons.lang3.text.WordUtils;

public class StringUtil {
	
	public static String nvl(String str) {
		return ((str == null) ? "" : str.trim());

	}

	public static String nvl(Object obj) {
		return ((obj == null) ? "" : obj.toString().trim());

	}

	public static String decode(String one, String another) {
		return ((one != null) ? one : another);

	}

	public static String decode(boolean test, String one, String another) {
		return ((test) ? one : another);

	}

	public static String lpad(String text, char fill, int length) {
		return pad(text, fill, length, true);

	}

	public static String rpad(String text, char fill, int length) {
		return pad(text, fill, length, false);

	}

	public static String pad(String text, char fill, int length, boolean left) {
		if (text.length() >= length) {
			return text;
		}

		String complete = "";

		for (int i = 0; i < length - text.length(); ++i) {
			complete = complete + fill;

		}

		if (left) {
			return complete + text;

		}

		return text + complete;

	}

	public static boolean isEmpty(String str) {
		return ((str == null) || (str.trim().length() <= 0));

	}

	public static String normalizaCaminhoDeDiretorio(String fullDirPath) {
		String folder = fullDirPath;

		if ((!("".equals(fullDirPath))) && (fullDirPath != null)) {
			folder = fixInitiOfString(folder);

			folder = removeSlashsFromEndOfString(folder);

		}

		return folder;

	}

	private static String fixInitiOfString(String str) {
		StringBuilder ret = new StringBuilder(str);

		while (ret.toString().startsWith("/")) {
			ret = new StringBuilder(ret.toString().substring(1, ret.toString().length()));

		}

		return "/" + ret.toString();

	}

	private static String removeSlashsFromEndOfString(String str) {
		StringBuilder ret = new StringBuilder(str);

		while (ret.toString().endsWith("/")) {
			ret = new StringBuilder(ret.toString().substring(0, ret.toString().length() - 1));

		}

		return ret.toString();

	}

	public static String ajustarTextoReleaseTO(String texto) {
		texto = texto.replaceAll("'"," ");
		texto = texto.replaceAll("-"," ");
		for (int i = 0 ; i < 20 ; i++) {
			texto = texto.replace("(" + i + ")","  ");
		}
		texto = texto.replace("."," ");
		texto = texto.replaceAll("  "," ");
		texto = texto.toLowerCase();
		texto = texto.trim();
		return ajustarTextoMaiusculo(texto);
	}

	public static String ajustarTextoMaiusculo(String texto) {
		if (texto.indexOf(" (") != -1) { return WordUtils.capitalize(texto.substring(0,texto.indexOf(" ("))) + " (" + WordUtils.capitalize(texto.substring(texto.indexOf(" (") + 2,texto.length())); }
		return WordUtils.capitalize(texto);
	}

	public static String removerAcentos(String valor) {
		return Normalizer.normalize(valor,Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
	}
	
	public static String ajustarArtistaMusica(String valor) {
				
		valor = valor.replace(",", "&");
		
		valor = valor.replace("?", " ");
		
		valor = valor.replace("#", " ");
		
		valor = valor.replace(".", " ");
		
		valor = valor.replace("'", " ");
		
		valor = valor.replace("  "," ");
		
		valor = removerAcentos(valor);
		
		return ajustarTextoMaiusculo(valor);
		
	}

	public static String removerNumeroInicialMusicaElectrobuzz(String valor) {
		for (int i = 0; i < 21; i++) {
			valor = remover(i, "-", valor);
			valor = remover(i, "_", valor);
		}
		return valor;
	}
	
	public static String remover(int i, String separador, String valor) {
		String numero = (i < 10 ? "0" + i + separador: "" + i + separador);
		if (valor.indexOf(numero) != -1) {
			valor = valor.replace(numero, "");
		}
		return valor;
	}
	
	public static String removerLast(String valor, String valorInicio, String valorFim) {
		int inicio = valor.lastIndexOf(valorInicio);
		int fim = valor.lastIndexOf(valorFim) + 1;
		if (inicio != -1 && fim != -1) {
			valor = valor.replace(valor.substring(inicio, fim), "");
		}
		return valor;
	}
	
	public static String ajustarArquivo(String arquivo, boolean electrobuzz, boolean ajustaAspasFinais) {
		
		if (electrobuzz) {
			arquivo = removerNumeroInicialMusicaElectrobuzz(arquivo);
			if (ajustaAspasFinais) {
				arquivo = removerLast(arquivo, "(", ")");
			}
		}
		
		arquivo = arquivo.replaceAll(", "," & ");
		arquivo = arquivo.replace("-(electrobuzz.net)", "");		
		arquivo = StringUtil.remover(arquivo,"[","]");
		
		arquivo = arquivo.toLowerCase();
		arquivo = arquivo.replaceAll(".mp3","");
		
		arquivo = arquivo.replace("vinyl only", "");

		// - PALAVRAS EXCLUSIVAS - //
		arquivo = arquivo.replace("kaï¿½par","Kaspar");
		arquivo = arquivo.replace("dj w!ld","dj wild");
		arquivo = arquivo.replace("e-culture","e culture");
		arquivo = arquivo.replace("(myfreemp3.eu)","");
		arquivo = arquivo.replace("(original)","");
		arquivo = arquivo.replace("Wwwhtrkorg","");
		arquivo = arquivo.replace("[wwwfiber.com]","");
		arquivo = arquivo.replace("-www.htrk.net","");
		// - PALAVRAS EXCLUSIVAS - //

		arquivo = arquivo.replaceAll("_"," ");
		arquivo = arquivo.replace('?',' ');
		arquivo = arquivo.replace('!',' ');
		arquivo = arquivo.replace('#',' ');
		arquivo = arquivo.replace(" & "," ??? ");
		arquivo = arquivo.replace('&',' ');
		arquivo = arquivo.replace(" ??? "," & ");

		arquivo = arquivo.replaceAll("'"," ");
		arquivo = arquivo.replaceAll("- www.electrobuzz.net","");
		arquivo = arquivo.replaceAll("-electrobuzz.net","");
		arquivo = arquivo.replaceAll("-electrobuzz","");
		arquivo = arquivo.replace(".","");

		// arquivo = ajustar("a",arquivo);
		// arquivo = ajustar("b",arquivo);
		// arquivo = ajustar("c",arquivo);
		// arquivo = ajustar("d",arquivo);
		// arquivo = ajustar("e",arquivo);
		// arquivo = ajustar("f",arquivo);
		// arquivo = ajustar("g",arquivo);
		// arquivo = ajustar("h",arquivo);
		// arquivo = ajustar("i",arquivo);
		// arquivo = ajustar("",arquivo);

		arquivo = arquivo.replace("(org mix)","");
		arquivo = arquivo.replace("(original mix)","");
		arquivo = arquivo.replace("(original dub mix)","");
		arquivo = arquivo.replace("(original)","");
		arquivo = arquivo.replace(" original","");
		arquivo = arquivo.replace(" remix","");
		arquivo = arquivo.replace(" rmx","");
		arquivo = arquivo.replace(" mix","");
		arquivo = arquivo.replace(" edit","");

		arquivo = arquivo.replaceAll("--","-");
		arquivo = arquivo.replaceFirst("-"," ??? ");
		arquivo = arquivo.replaceAll("-"," ");
		arquivo = arquivo.replace(" ??? "," - ");
		arquivo = arquivo.replaceAll("  "," ");
		
		arquivo = arquivo.replaceAll(", "," & ");
		
		return StringUtil.ajustarTextoMaiusculo(arquivo).trim() + ".mp3";
	}

	public static String ajustar(String letra,String arquivo) {
		arquivo = arquivo.replaceAll(letra + "016-","");
		arquivo = arquivo.replaceAll(letra + "015-","");
		arquivo = arquivo.replaceAll(letra + "014-","");
		arquivo = arquivo.replaceAll(letra + "013-","");
		arquivo = arquivo.replaceAll(letra + "012-","");
		arquivo = arquivo.replaceAll(letra + "011-","");
		arquivo = arquivo.replaceAll(letra + "010-","");
		arquivo = arquivo.replaceAll(letra + "09-","");
		arquivo = arquivo.replaceAll(letra + "08-","");
		arquivo = arquivo.replaceAll(letra + "07-","");
		arquivo = arquivo.replaceAll(letra + "06-","");
		arquivo = arquivo.replaceAll(letra + "05-","");
		arquivo = arquivo.replaceAll(letra + "04-","");
		arquivo = arquivo.replaceAll(letra + "03-","");
		arquivo = arquivo.replaceAll(letra + "02-","");
		arquivo = arquivo.replaceAll(letra + "01-","");
		arquivo = arquivo.replaceAll(letra + "00-","");
		arquivo = arquivo.replaceAll(letra + "16-","");
		arquivo = arquivo.replaceAll(letra + "15-","");
		arquivo = arquivo.replaceAll(letra + "14-","");
		arquivo = arquivo.replaceAll(letra + "13-","");
		arquivo = arquivo.replaceAll(letra + "12-","");
		arquivo = arquivo.replaceAll(letra + "11-","");
		arquivo = arquivo.replaceAll(letra + "10-","");
		arquivo = arquivo.replaceAll(letra + "9-","");
		arquivo = arquivo.replaceAll(letra + "8-","");
		arquivo = arquivo.replaceAll(letra + "7-","");
		arquivo = arquivo.replaceAll(letra + "6-","");
		arquivo = arquivo.replaceAll(letra + "5-","");
		arquivo = arquivo.replaceAll(letra + "4-","");
		arquivo = arquivo.replaceAll(letra + "3-","");
		arquivo = arquivo.replaceAll(letra + "2-","");
		arquivo = arquivo.replaceAll(letra + "1-","");
		arquivo = arquivo.replaceAll(letra + "0-","");
		arquivo = arquivo.replaceAll(letra + "014 - ","");
		arquivo = arquivo.replaceAll(letra + "013 - ","");
		arquivo = arquivo.replaceAll(letra + "012 - ","");
		arquivo = arquivo.replaceAll(letra + "011 - ","");
		arquivo = arquivo.replaceAll(letra + "010 - ","");
		arquivo = arquivo.replaceAll(letra + "09 - ","");
		arquivo = arquivo.replaceAll(letra + "08 - ","");
		arquivo = arquivo.replaceAll(letra + "07 - ","");
		arquivo = arquivo.replaceAll(letra + "06 - ","");
		arquivo = arquivo.replaceAll(letra + "05 - ","");
		arquivo = arquivo.replaceAll(letra + "04 - ","");
		arquivo = arquivo.replaceAll(letra + "03 - ","");
		arquivo = arquivo.replaceAll(letra + "02 - ","");
		arquivo = arquivo.replaceAll(letra + "01 - ","");
		arquivo = arquivo.replaceAll(letra + "00 - ","");
		arquivo = arquivo.replaceAll(letra + "14 - ","");
		arquivo = arquivo.replaceAll(letra + "13 - ","");
		arquivo = arquivo.replaceAll(letra + "12 - ","");
		arquivo = arquivo.replaceAll(letra + "11 - ","");
		arquivo = arquivo.replaceAll(letra + "10 - ","");
		arquivo = arquivo.replaceAll(letra + "9 - ","");
		arquivo = arquivo.replaceAll(letra + "8 - ","");
		arquivo = arquivo.replaceAll(letra + "7 - ","");
		arquivo = arquivo.replaceAll(letra + "6 - ","");
		arquivo = arquivo.replaceAll(letra + "5 - ","");
		arquivo = arquivo.replaceAll(letra + "4 - ","");
		arquivo = arquivo.replaceAll(letra + "3 - ","");
		arquivo = arquivo.replaceAll(letra + "2 - ","");
		arquivo = arquivo.replaceAll(letra + "1 - ","");
		arquivo = arquivo.replaceAll(letra + "0 - ","");
		return arquivo;
	}
	
	public static String gerarQuery(String valor) {
		
		return valor.replaceAll(" & "," ").replace("(","").replace(")","").replace(" - "," ").toLowerCase().replaceAll(" ","+");
		
	}
	
	public static String remover(String valor,String valorInicio,String valorFim) {

		int inicio = valor.indexOf(valorInicio);

		int fim = valor.indexOf(valorFim) + 1;

		if (inicio != -1 && fim != -1) {

			valor = valor.replace(valor.substring(inicio,fim),"");

		}

		return valor;

	}
	
	public static String getValor(String valor,String valorInicio,String valorFim) {
		
		int inicio = valor.indexOf(valorInicio);

		int fim = valor.indexOf(valorFim) + 1;
		
		if (inicio != -1 && fim != -1) {
			
			return valor.substring(inicio + 1,fim - 1);
			
		}
		
		return null;
		
	}

}
