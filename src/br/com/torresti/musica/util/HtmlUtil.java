package br.com.torresti.musica.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.torresti.spring.exception.BusinessException;

public class HtmlUtil {
	
	public static String getHttpURLConnection(String valroURL) throws BusinessException {

		try {

			URL url = new URL(valroURL);
			
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			
			httpURLConnection.setRequestMethod("GET");
			
			httpURLConnection.setRequestProperty("User-Agent","Mozilla/5.0");

			InputStream inputStream = httpURLConnection.getInputStream();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

			StringBuilder stringBuilderJSON = new StringBuilder();
			
			bufferedReader.lines().forEach(l -> stringBuilderJSON.append(l.trim()));
			
			return stringBuilderJSON.toString();

		} catch (Exception e) {
			
			throw new BusinessException(e);
			
		}

	}
	
	public static String removeTag(String valor) {

		return removeTag(valor, "<", ">");

	}
	public static String removeTag(String valor, String char01, String char02) {

		while (valor.indexOf(char01) != -1) {

			valor = valor.replace(char01 + valor.substring(valor.indexOf(char01) + 1, valor.indexOf(char02)) + char02,"");

		}

		return valor;

	}

	public static String removeValor(String valor, String charInicial) {

		if (valor.indexOf(charInicial) != -1) {
		
			valor = valor.replace(valor.substring(valor.indexOf(charInicial), valor.length()), "");
			
		}
		
		return valor;

	}
	
	public static String alterarHtmlEncode(String valor) {
		
		valor = valor.replaceAll(" &amp; ", " & ");
		
		return valor;
		
	}
	
	

}
