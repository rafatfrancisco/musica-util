package br.com.torresti.musica.util.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.torresti.spring.to.RestTO;

public class PesquisaDiscoDTO extends RestTO {

	private Integer quantidade = 1;

	private String id;

	private String codigoUrl;

	private String selo;

	private String artista;

	private String musica;

	private List<ResultadoDiscoDTO> listaResultadoDiscoDTO;

	public PesquisaDiscoDTO() {
		listaResultadoDiscoDTO = new ArrayList<>();
	}

	public PesquisaDiscoDTO(RestTO restTO) {
		super(restTO.getCodigo(), restTO.getMensagem());
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigoUrl() {
		return codigoUrl;
	}

	public void setCodigoUrl(String codigoUrl) {
		this.codigoUrl = codigoUrl;
	}

	public String getSelo() {
		return selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public List<ResultadoDiscoDTO> getListaResultadoDiscoDTO() {
		return listaResultadoDiscoDTO;
	}

	public void setListaResultadoDiscoDTO(List<ResultadoDiscoDTO> listaResultadoDiscoDTO) {
		this.listaResultadoDiscoDTO = listaResultadoDiscoDTO;
	}

}
