package br.com.torresti.musica.util.dto;

import java.util.List;

public class CadastroDescksDTO {

	private List<String> listaUrlDecks;

	public List<String> getListaUrlDecks() {
		return listaUrlDecks;
	}

	public void setListaUrlDecks(List<String> listaUrlDecks) {
		this.listaUrlDecks = listaUrlDecks;
	}

}
