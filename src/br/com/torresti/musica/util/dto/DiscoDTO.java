package br.com.torresti.musica.util.dto;

import java.util.List;

public class DiscoDTO {

	private Integer idDisco;

	private String active;

	private Integer ano;

	private String nome;

	private String selo;

	private Integer tipo;

	private String urlSite;

	private String urlImagem;
	
	private Integer situacao;

	private Integer idPrateleira;
	
	private String dataCriacao;

	private List<MusicaDiscoDTO> listaMusicaDiscoDTO;

	public Integer getIdDisco() {
		return idDisco;
	}

	public void setIdDisco(Integer idDisco) {
		this.idDisco = idDisco;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSelo() {
		return selo;
	}

	public void setSelo(String selo) {
		this.selo = selo;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public String getUrlSite() {
		return urlSite;
	}

	public void setUrlSite(String urlSite) {
		this.urlSite = urlSite;
	}

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
	
	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Integer getIdPrateleira() {
		return idPrateleira;
	}

	public void setIdPrateleira(Integer idPrateleira) {
		this.idPrateleira = idPrateleira;
	}
	
	public String getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(String dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public List<MusicaDiscoDTO> getListaMusicaDiscoDTO() {
		return listaMusicaDiscoDTO;
	}

	public void setListaMusicaDiscoDTO(List<MusicaDiscoDTO> listaMusicaDiscoDTO) {
		this.listaMusicaDiscoDTO = listaMusicaDiscoDTO;
	}

}
