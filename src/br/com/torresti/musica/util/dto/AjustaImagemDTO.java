package br.com.torresti.musica.util.dto;

public class AjustaImagemDTO {

	private Integer idDisco;
	
	private String url;
	
	private String mensagem;

	public Integer getIdDisco() {
		return idDisco;
	}

	public void setIdDisco(Integer idDisco) {
		this.idDisco = idDisco;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}
