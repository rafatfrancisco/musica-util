package br.com.torresti.musica.util;

public class MusicaConstantes {
	
	public static enum TipoSite {

		Discogs(1), 
		Decks(2), 
		Beatport(3),
		Juno(4),
		DeejayDe(5);
		
		private Integer tipo;
		
		private TipoSite(Integer tipo) {
			this.tipo = tipo;
		}
		
		public Integer getTipo() {
			return tipo;
		}
		
		public static TipoSite getTipoSite(Integer tipo){
			for (TipoSite tipoAlbum : TipoSite.values()) {
				if (tipoAlbum.getTipo().equals(tipo)) {
					return tipoAlbum;
				}
			}
			return null;
		}
		
	}

}
