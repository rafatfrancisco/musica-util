package br.com.torresti.musica.util;

public class DecksUtil {

	public static String ajustarArtista(String valor) {
		
		valor = StringUtil.ajustarTextoReleaseTO(valor);
		
		valor = HtmlUtil.alterarHtmlEncode(valor);
		
		return StringUtil.ajustarArtistaMusica(valor);
		
	}
	
	public static String ajustarArtistaMusica(String valor) {
		
		valor = HtmlUtil.removeTag(valor);
		
		valor = HtmlUtil.alterarHtmlEncode(valor);
		
		valor = valor.toLowerCase();
		
		valor = removerLado("a", valor);
		
		valor = removerLado("b", valor);
		
		valor = removerLado("c", valor);
		
		valor = removerLado("d", valor);
		
		valor = removerLado("e", valor);
		
		valor = removerLado("f", valor);
		
		valor = removerLado("g", valor);
		
		valor = removerLado("h", valor);
		
		valor = removerNumeroMusica(valor);
		
		return StringUtil.ajustarArtistaMusica(valor);
		
	}
	
	public static String removerLado(String lado, String valor) {
		
		for (int i = 1; i <= 4; i++) {
			
			valor = valor.replace(lado + i + ": ","");
			
		}
		
		return valor;
		
	}
	
	public static String removerNumeroMusica(String valor) {
		
		for (int i = 1; i <= 20; i++) {
			
			valor = valor.replace(i + ". ","");
			
		}
		
		return valor;
		
	}

}
