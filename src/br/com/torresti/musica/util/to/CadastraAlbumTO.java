package br.com.torresti.musica.util.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CadastraAlbumTO {

	@JsonProperty(value = "urlDecks")
	private String urlDecks;

	public String getUrlDecks() {
		return urlDecks;
	}

	public void setUrlDecks(String urlDecks) {
		this.urlDecks = urlDecks;
	}

}
