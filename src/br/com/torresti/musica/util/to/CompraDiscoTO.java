package br.com.torresti.musica.util.to;

import java.util.List;

public class CompraDiscoTO {

	private String data;
	
	private Integer tipo;

	private Integer idPrateleira;

	private List<String> listaId;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Integer getIdPrateleira() {
		return idPrateleira;
	}

	public void setIdPrateleira(Integer idPrateleira) {
		this.idPrateleira = idPrateleira;
	}

	public List<String> getListaId() {
		return listaId;
	}

	public void setListaId(List<String> listaId) {
		this.listaId = listaId;
	}

}
