package br.com.torresti.spring.to;

public class ViewServiceTO<E> extends BaseTO {

	private E dto;

	public E getDto() {
		return dto;
	}

	public void setDto(E dto) {
		this.dto = dto;
	}
	
}
