package br.com.torresti.spring.to;

import br.com.torresti.spring.util.SituacaoRest;

public class AjaxTO<T> extends BaseTO {

	private T body;
	
	private Long retorno; 

	public static <T> AjaxTO<T> sucesso() {
		return new AjaxTO<T>(null, SituacaoRest.Ok.getCodigo());
	}
	
	public static <T> AjaxTO<T> sucesso(T body) {
		return new AjaxTO<T>(body, SituacaoRest.Ok.getCodigo());
	}

	public static <T> AjaxTO<T> sucesso(String mensagemSucesso) {
		return new AjaxTO<T>(null, SituacaoRest.Ok.getCodigo(), mensagemSucesso, null);
	}

	public static <T> AjaxTO<T> sucesso(T body, String mensagemSucesso) {
		return new AjaxTO<T>(body, SituacaoRest.Ok.getCodigo(), mensagemSucesso, null);
	}

	public static <T> AjaxTO<T> erro(String mensagemErro) {
		return new AjaxTO<T>(null, SituacaoRest.Erro.getCodigo(), null, mensagemErro);
	}
	
	public static <T> AjaxTO<T> erro(T body, String mensagemErro) {
		return new AjaxTO<T>(body, SituacaoRest.Erro.getCodigo(), null, mensagemErro);
	}

	protected AjaxTO(T body, Long retorno) {
		this(body, retorno, null, null);
	}

	protected AjaxTO(T body, Long retorno, String mensagemSucesso, String mensagemErro) {
		this.body = body;
		this.retorno = retorno;
		this.setMensagemSucesso(mensagemSucesso);
		this.setMensagemErro(mensagemErro);
	}

	public T getBody() {
		return body;
	}

	public Long getRetorno() {
		return retorno;
	}

	public void setRetorno(Long retorno) {
		this.retorno = retorno;
	}
	
}
