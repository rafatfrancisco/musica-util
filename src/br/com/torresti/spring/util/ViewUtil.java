package br.com.torresti.spring.util;

import br.com.torresti.musica.util.StringUtil;
import br.com.torresti.spring.exception.BusinessException;

public class ViewUtil {

	public static void isNull(Object value, String msg) throws BusinessException {
		if (value == null) throw new BusinessException(msg);
	}
	
	public static void isNotNull(Object value, String msg) throws BusinessException {
		if (value != null) throw new BusinessException(msg);
	}
	
	public static void isNullorZero(Long value, String msg) throws BusinessException {
		if (value == null || value == 0L) throw new BusinessException(msg);
	}
	
	public static void isEmpty(String value, String msg) throws BusinessException {
		if (StringUtil.isEmpty(value)) throw new BusinessException(msg);
	}
	
}
