package br.com.torresti.spring.util;

public enum SituacaoRest {

	Ok(0L), Erro(1L);

	private Long codigo;

	private SituacaoRest(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

}
