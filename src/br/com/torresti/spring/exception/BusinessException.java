package br.com.torresti.spring.exception;

/**
 * The Class BusinessException.
 */
@SuppressWarnings("serial")
public class BusinessException extends RuntimeException {

	/**
	 * Instantiates a new business exception.
	 *
	 * @param msg the msg
	 */
	public BusinessException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param msg the msg
	 * @param t the t
	 */
	public BusinessException(String msg,Throwable t) {
		super(msg,t);
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param t the t
	 */
	public BusinessException(Throwable t) {
		super(t);
	}

}
